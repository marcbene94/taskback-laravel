<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lista extends Model
{
  protected $table = "lista";

  private $miarray = array('hola', 'que tal?');

  public function getMiArray() {
    return $this->miarray;
  }
}