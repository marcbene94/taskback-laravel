<?php

namespace App\Http\Controllers;

use App\Models\Lista;

use Illuminate\Support\Facades\DB;

class ListaController extends Controller
{
    private $lista;

    private function crearInstanciaClaseLista() {
        $this->lista = new Lista();
    }

    public function obtenerListas()
    {
        $this->crearInstanciaClaseLista();   
        $listas = Lista::select('title')->pluck('title');
        return $listas;
    }

    public function obtenerArrayPrueba()
    {
        $this->crearInstanciaClaseLista();
        return $this->lista->getMiArray();
    }
}