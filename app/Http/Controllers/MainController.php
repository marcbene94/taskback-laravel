<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Lista;

use App\Models\Tarjeta;

class MainController extends Controller
{
    private $lista;
    private $tarjeta;
    private $listas;
    private $tarjetas;
    private $response;

    private function crearInstanciaClaseLista() {
        $this->lista = new Lista();
    }

    private function crearInstanciaClaseTarjeta() {
        $this->tarjeta = new Tarjeta();
    }

    public function getAllData()
    {
        $this->crearInstanciaClaseLista(); 
        $this->crearInstanciaClaseTarjeta();

        $this->listas = Lista::select(array('id','title'))->where('active','1')->get();
        $this->tarjetas = Tarjeta::select(array('id','title'))->where('active','1')->get();

        $this->response = array(
            'data_lists' => $this->listas,
            'data_cards' => $this->tarjetas,
        );

        return $this->response;
    }
}
